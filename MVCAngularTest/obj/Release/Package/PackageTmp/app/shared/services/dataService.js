﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('dataService', ['$http', '$q', function ($http, $q) {
            var service = {};
            service.getUsers = function () {
                var deferred = $q.defer();
                $http.get('/User/Index').then(function (result) {
                    deferred.resolve(result.data);
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };
            service.getUser = function (ID) {
                var deferred = $q.defer();
                $http.post('/User/GetUser', { ID: ID }).then(function (result) {
                    deferred.resolve(result.data);
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };
            service.saveUser = function (Model) {
                var deferred = $q.defer();
                $http.post('/User/CreateUser', {pModel : Model}).then(function (result) {
                    deferred.resolve(result.data);
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };
            service.updateUser = function (Model) {
                var deferred = $q.defer();
                $http.post('/User/UpdateUser', { objUser: Model }).then(function (result) {
                    deferred.resolve(result.data);
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };
            service.deleteUser = function (id) {
                var deferred = $q.defer();
                $http.post('/User/DeleteUser', { id: id }).then(function (result) {
                    deferred.resolve(result.data);
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };
            return service;
        }]);


})();