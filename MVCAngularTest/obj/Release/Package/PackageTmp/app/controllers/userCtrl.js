﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('userCtrl', ['$scope', 'dataService', '$location', function ($scope, dataService, $location) {
            $scope.users = [];
            getData();
            $scope.ID = '';
            $scope.model = {};
            function getData() {
                dataService.getUsers().then(function (result) {
                    $scope.users = result;
                })
            }
            $scope.redirectView = function () {
                $location.path("/viewUser");
            }
            $scope.redirectCreate = function () {
                $location.path("/createUser");
            }
            $scope.redirectUpdate = function (id) {
                $location.path("/updateUser/" + id);
            }
            $scope.redirectDelete = function (id) {
                dataService.deleteUser(id).then(function (result) {
                    if (result == true)
                        alert("Deleted Successfully!");
                    else
                        alert("Delete Failed!")
                    getData();
                })
            }
        }]);

})();
