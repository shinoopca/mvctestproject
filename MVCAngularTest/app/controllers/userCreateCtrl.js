﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('userCreateCtrl', ['$scope', 'dataService', '$location', function ($scope, dataService, $location) {
            $scope.model = {};
            $scope.redirectView = function () {
                $location.path("/viewUser");
            }
            $scope.redirectCreate = function () {
                $location.path("/createUser");
            }
            $scope.redirectUpdate = function () {
                $location.path("/updateUser");
            }
            $scope.redirectDelete = function () {
                $location.path("/viewUser");
            }
            $scope.saveUser = function () {
                dataService.saveUser($scope.model).then(function (result) {
                    $scope.model = {};
                    if (result == true)
                        alert("Saved Successfully!");
                    else
                        alert("Save Failed!")
                    $location.path("/viewUser");
                })
            }
        }]);
})();
