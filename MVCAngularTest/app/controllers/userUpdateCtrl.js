﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('userUpdateCtrl', ['$scope', 'dataService', '$location', '$routeParams', function ($scope, dataService, $location, $routeParams) {
            $scope.User = {};
            $scope.id = $routeParams.id;
            getUser();
            function getUser() {
                dataService.getUser($scope.id).then(function (result) {
                    $scope.User = result;
                })
            }
            $scope.redirectView = function () {
                $location.path("/viewUser");
            }
            $scope.redirectCreate = function () {
                $location.path("/createUser");
            }
            $scope.redirectUpdate = function () {
                $location.path("/updateUser");
            }
            $scope.redirectDelete = function () {
                $location.path("/viewUser");
            }
            $scope.updateUser = function () {
                dataService.updateUser($scope.User).then(function (result) {
                    $scope.User = {};
                    alert("Updated Successfully!");
                    $location.path("/viewUser");
                })
            }
        }]);

})();
