﻿(function () {
    'use strict';

    angular.module('app', [
        'ngRoute'
    ]).config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'userCtrl',
                templateUrl: '/app/templates/user.html'
            })
            .when('/viewUser', {
                controller: 'userCtrl',
                templateUrl: '/app/templates/user.html'
            })
            .when('/createUser', {
                controller: 'userCreateCtrl',
                templateUrl: '/app/templates/userCreate.html'
            })
            .when('/updateUser/:id', {
                controller: 'userUpdateCtrl',
                templateUrl: '/app/templates/userUpdate.html'
            })
            .otherwise({ redirectTo: '/' });
    }]);
})();