﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using MVCAngularTest.Models.EF;
using Newtonsoft.Json;

namespace MVCAngularTest.BusinessLayer
{
    public partial class UserManagementJSON
    {
        private ValidationComponent objValidate = null;
        string FileFullPath = "";
        public UserManagementJSON()
        {
            objValidate = new ValidationComponent();
            string FileName = "UserData.json";
            string FilePath = HttpContext.Current.Request.PhysicalApplicationPath;
            FileFullPath = Path.Combine(FilePath, "bin", FileName);
        }
        public List<User> ReadUserDataJson()
        {
            if (!File.Exists(FileFullPath))
                return null;
            else
            {
                var jsonUserData = File.ReadAllText(FileFullPath);
                return JsonConvert.DeserializeObject<List<User>>(jsonUserData);
            }
        }
        public bool WriteUserDataJson(List<User> userData)
        {
            if (userData != null && userData.Count > 0)
            {
                var jsonUserData = JsonConvert.SerializeObject(userData, Formatting.Indented);
                File.WriteAllText(FileFullPath, jsonUserData);
                return true;
            }
            else
                return false;
        }
    }
}