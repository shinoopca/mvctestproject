﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCAngularTest.Models.EF;

namespace MVCAngularTest.BusinessLayer.Interfaces
{
    public interface IUserManager
    {
        List<User> ViewUsers();
        bool CreateUser(User objUser);
        bool UpdateUser(User objUser);
        bool DeleteUser(int id);
        User GetUser(int Id);
    }
}
