﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCAngularTest.Models.EF;
using MVCAngularTest.BusinessLayer.Interfaces;

namespace MVCAngularTest.BusinessLayer
{
    public class UserManagementDB : IUserManager
    {
        private AngularjsMvcDbContext db = null;
        private ValidationComponent objValidate = null;

        public UserManagementDB()
        {
            db = new AngularjsMvcDbContext();
            objValidate = new ValidationComponent();
        }

        public List<User> ViewUsers()
        {
            try
            {
                var users = db.Users.ToList();
                return users;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User GetUser(int Id)
        {
            try
            {
                var result = db.Users.SingleOrDefault(b => b.Id == Id);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CreateUser(User objUser)
        {
            try
            {
                if (!objValidate.ValidateUserData(objUser) || objUser == null)
                    return false;
                var user = db.Set<User>();
                user.Add(objUser);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }

        public bool UpdateUser(User objUser)
        {
            try
            {
                if (!objValidate.ValidateUserData(objUser) || objUser == null)
                    return false;
                var result = db.Users?.ToList()?.SingleOrDefault(b => objUser != null && b.Id == objUser.Id);
                if (result != null)
                {
                    result.Name = objUser.Name;
                    result.Age = objUser.Age;
                    result.Email = objUser.Email;
                    result.Address = objUser.Address;
                    result.Mobile = objUser.Mobile;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }

        public bool DeleteUser(int id)
        {
            try
            {
                var itemToRemove = db.Users.SingleOrDefault(x => x.Id == id);
                if (itemToRemove != null)
                {
                    db.Users.Remove(itemToRemove);
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }
    }
}