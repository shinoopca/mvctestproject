﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCAngularTest.Models.EF;

namespace MVCAngularTest.BusinessLayer
{
    public class ValidationComponent
    {
        public bool ValidateUserData(User objUser)
        {
            if (string.IsNullOrEmpty(objUser.Name) || objUser.Name.Length > 50)
                return false;
            else if (objUser.Age <= 0 || objUser.Age > 120)
                return false;
            else if (string.IsNullOrEmpty(objUser.Email) || objUser.Email.Length > 50)
                return false;
            else if (string.IsNullOrEmpty(objUser.Address) || objUser.Address.Length > 50)
                return false;
            else if (string.IsNullOrEmpty(objUser.Mobile) || objUser.Mobile.Length > 20)
                return false;
            return true;
        }
    }
}