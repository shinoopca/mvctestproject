﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCAngularTest.Models.EF;
using MVCAngularTest.BusinessLayer.Interfaces;

namespace MVCAngularTest.BusinessLayer
{
    public partial class UserManagementJSON : IUserManager
    {
        public List<User> ViewUsers()
        {
            try
            {
                var users = ReadUserDataJson();
                return users;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User GetUser(int Id)
        {
            try
            {
                var result = ReadUserDataJson().FirstOrDefault(b => b.Id == Id);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CreateUser(User objUser)
        {
            try
            {
                if (!objValidate.ValidateUserData(objUser) || objUser == null)
                    return false;
                int id = 0;
                var users = ReadUserDataJson();
                if (users != null && users.Count > 0)
                    id = users.Max(x => x.Id);
                else
                    users = new List<User>();
                objUser.Id = id + 1;
                users.Add(objUser);
                return WriteUserDataJson(users);
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }

        public bool UpdateUser(User objUser)
        {
            try
            {
                if (!objValidate.ValidateUserData(objUser) || objUser == null)
                    return false;
                var users = ReadUserDataJson();
                if (users != null && users.Count > 0)
                {
                    users.ForEach(x => {
                            if (x.Id == objUser.Id)
                            {
                                x.Name = objUser.Name;
                                x.Age = objUser.Age;
                                x.Address = objUser.Address;
                                x.Email = objUser.Email;
                                x.Mobile = objUser.Mobile;
                            }
                        }
                    );

                    return WriteUserDataJson(users);
                }
                return false;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }

        public bool DeleteUser(int id)
        {
            try
            {
                var users = ReadUserDataJson();
                if (users != null && users.Count() > 0)
                {
                    var user = users.FirstOrDefault(x => x.Id == id);
                    if (user == null)
                        return false;
                    users.Remove(user);
                    return WriteUserDataJson(users);
                }
                return false;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }
    }
}