﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCAngularTest.Models.EF;
using MVCAngularTest.BusinessLayer;
using MVCAngularTest.BusinessLayer.Interfaces;

namespace MVCAngularTest.Controllers
{
    public class UserController : Controller
    {
        public IUserManager userManager;

        public UserController()
        {
            userManager = new UserManagementJSON();
            //userManager = new UserManagementDB();     //Correct SQL ConnectionString in web config file and try.
            //userManager = new UserManagementXML();    //Not implemented.
        }

        public JsonResult Index()
        {
            var users = userManager.ViewUsers();
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUser(string ID)
        {
            var user = userManager.GetUser(Convert.ToInt32(ID));
            return Json(user, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateUser(User pModel)
        {
            return Json(userManager.CreateUser(pModel), JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateUser(User objUser)
        {
            return Json(userManager.UpdateUser(objUser), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteUser(string id)
        {
            if (Int32.TryParse(id, out int resId))
                return Json(userManager.DeleteUser(resId), JsonRequestBehavior.AllowGet);
            else
                return Json(false, JsonRequestBehavior.AllowGet);
        }
    }
}